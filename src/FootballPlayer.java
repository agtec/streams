import java.time.LocalDate;

public class FootballPlayer {
    private String name;
    private String lastName;
    private String position;
    private LocalDate dateOfBirth;

    public FootballPlayer(String name, String lastName, String position, LocalDate dateOfBirth) {
        this.name = name;
        this.lastName = lastName;
        this.position = position;
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        if (getDateOfBirth() == null) {
            return String.format("%s, %s, %s", getName(), getLastName(), getPosition());
        }
        return String.format("%s %s, %s, %s", getName(), getLastName(), getPosition(), getDateOfBirth());
    }
}