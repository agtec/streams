import java.util.List;

public class FootballTeam {
    private String name;
    private List<FootballPlayer> players;
    private FootballManager manager;
    private int age; //wiek klubu

    public FootballTeam(String name, List<FootballPlayer> players, FootballManager manager, int age) {
        this.name = name;
        this.players = players;
        this.manager = manager;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FootballPlayer> getPlayers() {
        return players;
    }

    public void setPlayers(List<FootballPlayer> players) {
        this.players = players;
    }

    public FootballManager getManager() {
        return manager;
    }

    public void setManager(FootballManager manager) {
        this.manager = manager;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s, %d", getName(), getPlayers(), getManager(), getAge());
    }
}