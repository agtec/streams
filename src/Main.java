import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException {

        ReadFile readFile = new ReadFile();
        FootballTeam footballTeam1;
        footballTeam1 = readFile.readFile("LegiaWarszawa.txt");

        FootballTeam footballTeam2;
        footballTeam2 = readFile.readFile("MKSPogońSzczecin.txt");

        List<FootballTeam> teams = Arrays.asList(footballTeam1, footballTeam2);

        FootballLeague footballLeague = new FootballLeague("Ekstraklasa", "Polska", 1, teams);
        List<FootballLeague> footballLeagues = Arrays.asList(footballLeague);

        System.out.println("All leagues managers: ");

        List<FootballManager> footballManagers = footballLeagues.stream()
                .flatMap(league -> {
                    return league.getTeams().stream()
                            .map(team -> {
                                return team.getManager();
                            });
                }).collect(Collectors.toList());

        footballManagers.stream().map(footballManager -> "- " + footballManager).forEach(System.out::println);

        List<FootballManager> footballManagers1 = footballLeagues.stream()
                .flatMap(league -> {
                    return league.getTeams().stream();
                })
                .map((team) -> {
                    return team.getManager();
                })
                .collect(Collectors.toList());


        List<FootballManager> footballManagersAge50 = footballLeagues.stream()
                .flatMap(league -> {
                    return league.getTeams().stream()
                            .map(team -> {
                                return team.getManager();
                            }).filter(manager -> {
                                ((FootballManager) manager).getDateOfBirth();
                                return LocalDate.now().minusYears(50).isBefore(((FootballManager) manager).getDateOfBirth());
                            });
                }).collect(Collectors.toList());

        footballManagersAge50.stream().map(footballManager -> "- " + footballManager).forEach(System.out::println);


        System.out.println("\nManagers from Austria: ");

        List<FootballManager> footballManagersFromAustria = footballLeagues.stream()
                .flatMap(league -> {
                    return league.getTeams().stream()
                            .map(team -> {
                                return team.getManager();
                            }).filter(manager -> {
                                return ((FootballManager) manager).getNationality().equalsIgnoreCase("Austria");
                            });
                }).collect(Collectors.toList());

        footballManagersFromAustria.stream().map(footballManager -> "- " + footballManager).forEach(System.out::println);


        System.out.println("\nAll football players: ");

        List<FootballPlayer> allFootballPlayers = footballLeagues.stream()
                .flatMap(league -> {
                    return league.getTeams().stream()
                            .flatMap(team -> {
                                return team.getPlayers().stream();
                            }).map(player -> {
                                return ((FootballPlayer) player);
                            });
                }).collect(Collectors.toList());

        allFootballPlayers.stream().map(player -> "- " + player).forEach(System.out::println);


        System.out.println("\nFootball players, whose position is attacker: ");

        List<FootballPlayer> footballPlayersAttackers = footballLeagues.stream()
                .flatMap(league -> {
                    return league.getTeams().stream()
                            .flatMap(team -> {
                                return team.getPlayers().stream();
                            }).map(player -> {
                                return ((FootballPlayer) player);
                            }).filter(player -> {
                                return player.getPosition().contains("apastnik");
                            });
                }).collect(Collectors.toList());

        footballPlayersAttackers.stream().map(player -> "- " + player).forEach(System.out::println);


        System.out.println("\nFootball players, whose position is a defender and they are max 30 years old: ");

        List<FootballPlayer> footballPlayersDefeders = footballLeagues.stream()
                .flatMap(league -> {
                    return league.getTeams().stream()
                            .flatMap(team -> {
                                return team.getPlayers().stream();
                            }).filter(player -> {
                                return player.getDateOfBirth() != null;
                            }).filter(player -> {
                                return LocalDate.now().minusYears(30).isBefore(player.getDateOfBirth());
                            }).filter(player -> {
                                return player.getPosition().contains("brońca");
                            });
                }).collect(Collectors.toList());

        footballPlayersDefeders.stream().map(player -> "- " + player).forEach(System.out::println);

        System.out.println("\nFootball teams in league: ");

        List<String> footballTeams = footballLeagues.stream()
                .flatMap(league -> {
                    return league.getTeams().stream();
                }).map(team -> {
                    return team.getName();
                }).collect(Collectors.toList());

        footballTeams.stream().forEach(System.out::println);


        System.out.println("\nPolish football teams: ");

        List<String> polishClubs = footballLeagues.stream()
                .filter(league -> {
                    return league.getCountry().equals("Polska");
                }).flatMap(league -> {
                    return league.getTeams().stream();
                }).map(team -> {
                    return team.getName();
                }).collect(Collectors.toList());

        polishClubs.stream().forEach(System.out::println);

        System.out.println("\nFootball teams that are more than 50 years old");

        List<String> teamsAtAge50 = footballLeagues.stream()
                .flatMap(league -> {
                    return league.getTeams().stream();
                }).filter(team -> {
                    return team.getAge() >= 50;
                }).map(team -> {
                    return team.getName();
                }).collect(Collectors.toList());

        teamsAtAge50.stream().forEach(System.out::println);

        System.out.println("\nFootball teams that have at least 2 football players: ");

        List<String> teamsWith2Players = footballLeagues.stream()
                .flatMap(league -> {
                    return league.getTeams().stream();
                }).filter(team -> {
                    return team.getPlayers().size() >= 2;
                }).map(team -> {
                    return team.getName();
                }).collect(Collectors.toList());

        teamsWith2Players.stream().forEach(System.out::println);

    }
}