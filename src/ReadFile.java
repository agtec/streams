import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ReadFile {

    public FootballTeam readFile(String filename) throws IOException {

        FootballTeam footballTeam = null;

        BufferedReader in = null;

        try {
            in = new BufferedReader(new FileReader(filename));

            String dataOfClub = in.readLine();
            String nameOfClub = dataOfClub.substring(6, dataOfClub.length());

            String[] ageData = in.readLine().split(" ");
            String ageOfClubStr = ageData[ageData.length - 1];
            int ageOfClub = Integer.parseInt(ageOfClubStr);

            String leageData = in.readLine();
            String league = leageData.substring(1, leageData.length());

            String countryData = in.readLine();
            String country = countryData.substring(9, countryData.length());

            String[] levelData = in.readLine().split(" ");
            String levelStr = levelData[levelData.length - 1];
            int level = Integer.parseInt(levelStr);

            String[] managerData = in.readLine().split(" ");
            String yearStr = managerData[managerData.length - 1];
            int managerYear = Integer.parseInt(yearStr);
            String monthStr = managerData[managerData.length - 2];
            int managerMonth = Integer.parseInt(monthStr);
            String dayStr = managerData[managerData.length - 3];
            int managerDay = Integer.parseInt(dayStr);
            LocalDate managerBirthDate = LocalDate.of(managerYear, managerMonth, managerDay);
            String managerCountry = managerData[managerData.length - 4];
            String managerName = managerData[1];
            String managerLastName = "";

            // gdyby nazwiska byly dwa
            for (int i = 2; i < managerData.length - 4; i++) {
                managerLastName = managerLastName + managerData[i] + " ";
            }
            managerLastName = managerLastName.trim();

            FootballManager footballManager = new FootballManager(managerName, managerLastName, managerCountry, managerBirthDate);

            List<FootballPlayer> footballPlayers = new ArrayList<>();

            String s;

            while ((s = in.readLine()) != null) {
                String[] playerData = s.split(" ");
                String playerYearStr = playerData[playerData.length - 1];
                int playerYear = Integer.parseInt(playerYearStr);
                String playerMonthStr = playerData[playerData.length - 2];
                int playerMonth = Integer.parseInt(playerMonthStr);
                String playerDayStr = playerData[playerData.length - 3];
                int playerDay = Integer.parseInt(playerDayStr);
                String position = playerData[playerData.length - 4];
                LocalDate playerBirthDate = null;
                if (playerYear != 0) {
                    playerBirthDate = LocalDate.of(playerYear, playerMonth, playerDay);
                }
                String playerName = playerData[0];
                String playerLastName = "";
                for (int i = 1; i < playerData.length - 4; i++) {
                    playerLastName = playerLastName + playerData[i];
                }

                FootballPlayer fb = new FootballPlayer(playerName, playerLastName, position, playerBirthDate);
                footballPlayers.add(fb);
            }

            footballTeam = new FootballTeam(nameOfClub, footballPlayers, footballManager, ageOfClub);

        } finally {

            if (in != null) {
                in.close();
            }
        }
        return footballTeam;
    }

}